package Whileloop;

import java.util.Scanner;

public class factorialCal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the factorial number");
		int number =sc.nextInt();
		int currentNum =number;
		int factorial =1 ;
		while(currentNum > 0){
			factorial = factorial *currentNum;
			currentNum--;
		}
		System.out.println("Factorial of "+number+" is "+ factorial);
	}

}
