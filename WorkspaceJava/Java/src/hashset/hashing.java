package hashset;

import java.util.HashSet;
import java.util.Iterator;

public class hashing {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Syntex for creating the hashset
		HashSet<Integer> set =new HashSet<>();
		//insert elements  into the set..-->set.add(element);
		set.add(1);
		set.add(2);
		set.add(3);
		set.add(1);
		set.add(5);
		set.add(12);
		set.add(98);
		
		//to check the size ;
		System.out.println("size of the set :"+set.size());
		
		//Print all elements
		System.out.println(set);
		
		//Iterator
		Iterator it =set.iterator();
		// this iterator has two main functions 1.hasNext & 2.next
		// at starting iterator points to null position when we use next it will check the set and point to 1 and so on...
		
		while(it.hasNext()){
			System.out.println(it.next());
		}
		
		// searching for the element in set by using contains keyword 
		
		if(set.contains(1)){
			System.out.println("set contains 1");
			
		}
		if(!set.contains(6)){
			System.out.println("set don't contains 6");
		}
		// delete 
		set.remove(1);
		if(!set.contains(1)){
			System.out.println("does not contain 1 -we delete 1 ");
		}
	}

}
