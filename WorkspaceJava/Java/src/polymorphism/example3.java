package polymorphism;
class Calculator{
	public int add(int num1, int num2){
		return num1+num2;
	}
	public double add(double num1 , double num2){
		return num1+num2;
	}
	
	
}
public class example3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Calculator cal = new Calculator();
		int result1 = cal.add(2, 3);
		double result2 = cal.add(2.5, 3.7);
		//cal.add(2, 3);
		System.out.println(result1);
		System.out.println(result2);
	}

}
