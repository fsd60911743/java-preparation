package polymorphism;

class Animal {
	public String name;
	public int age;
	public void animalSound(String name) {
		System.out.println(name);
		System.out.println("The animal makes a sound");
	}
	public void animalSound(int age) {
		System.out.println(age);
		System.out.println("The age of animal is 2 years");
	}
}

public class example2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Animal myAnimal = new Animal();
		myAnimal.name = "Dog";
		myAnimal.age = 2;
		//myAnimal.animalSound(myAnimal.name);
		myAnimal.animalSound(myAnimal.name);
		myAnimal.animalSound(myAnimal.age);
	}

}
