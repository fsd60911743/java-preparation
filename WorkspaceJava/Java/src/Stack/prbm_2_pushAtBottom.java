package Stack;

import java.util.Stack;

public class prbm_2_pushAtBottom {
	
	public static void pushAtBottom(int data , Stack<Integer> s){
		if(s.isEmpty()){
			s.push(data);
			return;
		}
		int top =s.pop();
		pushAtBottom(data ,s);
		s.push(top);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Stack<Integer> s = new Stack<>();
		s.push(1);
		s.push(2);
		s.push(3);
		//s.push(4);
		
		pushAtBottom(4 , s);
		while(!s.isEmpty()){
			System.out.println(s.peek());
			s.pop();
		}
	}

}
