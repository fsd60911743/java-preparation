package OOPS;

class Student1{
	String name;
	int age;
	
	public void printInfo(){
		System.out.println(this.name);
		System.out.println(this.age);
		
	}
	
//	Student1(){
//		System.out.println("constructor called");
//	}
}
public class example2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student1 s1 = new Student1();//student1() is called  constructor
		s1.name ="Pavan";
		s1.age = 24;
		
		s1.printInfo();
	}

}
// constructor is called we an object is created