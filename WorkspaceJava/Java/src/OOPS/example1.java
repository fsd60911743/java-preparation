package OOPS;

class Pen{
	String color;
	String type;
	
	public void write(){
		System.out.println("Writing something");
	}
	public void printColor(){
		System.out.println(this.color);
	}
	public void printType(){
		System.out.println(this.type);
	}
}

public class example1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Pen pen1 = new Pen();
		pen1.color = "blue";
		pen1.type ="gel";
		
		Pen pen2 = new Pen();
		pen2.color = "black";
		pen2.type = "ballPoint";
		
		pen1.printColor();
		pen2.printColor();
		
		pen1.printType();
		pen2.printType();
	}

}
