package OOPS;

public class objClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 // Objects and Classes in Java
//An object in java is the physical as well as logical entity, where as classnin java is a logical entity only

		// what is an object in Java
		// An entity that has state and behaviour is known as an object 
		//e.g: chair,bike, marker ,pen table,car etc.
		// it can be physical or logical (tangible and intangible) 
		//The example of an intangible object is the banking system
//		An object has three characteristics
//		State: represents the data (value) of an object.
//		Behavior: represents the behavior (functionality) of an object such as deposit, withdraw, etc.
//		Identity: An object identity is typically implemented via a unique ID. The value of the ID is not visible to the external user. However, it is used internally by the JVM to identify each object uniquely.
//		example Pen is an object ,its name is Reynolds, color is white , known as its state.it is used to write ,so writing is its behavior
		
//		An object is an instance of a class .A class is a tamplate or a blueprint from which object are created .So,anobject is the instance(result) of a class.
//		object Definitions
//		An object is a real world entity
//		an object is a runtime entity
//		the object is an entity which has state and behavior
//		the object is an instance of a class
	
//		What is a class in Java
//
//		A class is a group of objects which have common properties. It is a template or blueprint from which objects are created. It is a logical entity. It can't be physical.
//
//		A class in Java can contain:
//
//		Fields
//		Methods
//		Constructors
//		Blocks
//		Nested class and interface
		
//		Syntax to declare a class
//		 
//		class<class_name>{
//			field;
//			method;
//		}
		
//		Instance variable in Java
//
//		A variable which is created inside the class but outside the method is known as an instance variable. Instance variable doesn't get memory at compile time. It gets memory at runtime when an object or instance is created. That is why it is known as an instance variable.

	}

}
