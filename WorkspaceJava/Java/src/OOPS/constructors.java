package OOPS;

class Student{
	String name ;
	int age ;
	
	public void printInfo(){
		System.out.println(this.name);
		System.out.println(this.age);
	}
	// default constructor
//	Student(){
//		text.....
//	}
	// parametrized constructor
//	Student(String name , int age){ // parameterized constructors
//		this.name = name;
//		this.age = age;
//	}
	
	// copy constructor
	Student(Student s2){
		this.name = s2.name;
		this.age = s2.age;
	}
	Student (){
		
	}
}
public class constructors {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student s1 = new Student();
		s1.name="Pavan";
		s1.age =24;
		
		Student s2 =new Student(s1);
		s2.printInfo();
	}

}
