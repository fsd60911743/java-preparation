package twoDArrays;

import java.util.Scanner;

public class transpose {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the row and col size");
		int n = sc.nextInt();
		int m = sc.nextInt();
		
		int matrix[][] = new int [n][m];
		System.out.print("Enter the element of an matrix");
		for(int i = 0 ; i<n ; i++){
			for(int j = 0 ; j<m ; j++){
				matrix[i][j] = sc.nextInt();
			}
		}
		System.out.println("Transpose of matrix is");
		for(int j =0 ;j<m;j++){
			for(int i =0 ; i<n ;i++){
				System.out.print(matrix[i][j]+" ");
			}
			System.out.println();

		}
	}

}
