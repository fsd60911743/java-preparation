package abstraction;
abstract class Animal{
	abstract void walk();
	public void eat(){
		System.out.println("Animal eats");
	}
}
class Horse extends Animal{
	public void walk(){
		System.out.println("walks on 4 legs");
	}
}
class Chicken extends Animal{
	public void walk(){
		System.out.println("walks on 2 legs");
	}
	public void figit(){
		System.out.println("fight on sankranthi");
	}
}
public class example1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Horse horse = new Horse();
		horse.walk();
		Chicken chicken = new Chicken();
		chicken.walk();
		chicken.figit();
		chicken.eat();
	}

}
