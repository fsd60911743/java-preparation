package ArrayList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class sortingArrayList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> list = new ArrayList<>();
		
		System.out.println("Enter 5 integers");
		Scanner sc = new Scanner(System.in);
		for(int i = 0 ;i<10;i++){
			int temp = sc.nextInt();
			
			if( ! list.contains(temp))
				list.add(temp);
			
		}
		Collections.sort(list);
		System.out.print(list);
	}

}
