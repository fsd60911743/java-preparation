package ArrayList;

import java.util.ArrayList;

public class insertFristPosition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList <Integer> list = new ArrayList<>();
		list.add(1);
		list.add(3);
		list.add(5);
		list.add(7);
		System.out.println("list before insert : "+list);
		
		list.add(0, 99);
		System.out.println("list after insert at index 0 :"+list);
	}

}
