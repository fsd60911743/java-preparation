package ArrayList;

import java.util.ArrayList;

public class updateSpecificElement {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> list = new ArrayList<>();
		
		list.add("black");
		list.add("blue");
		list.add("green");
		list.add("Brown");
		list.add("yellow");
		
		System.out.println("list before updated :"+list);
		
		list.set(2, "pink");
		
		System.out.println("list after updated :"+list);
	}

}
