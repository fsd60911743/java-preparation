package ArrayList;

import java.util.ArrayList;
import java.util.Scanner;

// create a menu program with the following options
// 1.Add Element
// 2.Remove Element
// 3.Display Element
// 4.Exit
//This program runs infinitely until the user chooses fourth option.
public class menuCreation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		ArrayList<Integer> list = new ArrayList<>();
		Scanner sc = new Scanner(System.in);
		
		while(true){
			displayMenu();
			int choice = sc.nextInt();
			
			if(choice == 1){
				// add
				System.out.print("Enter an integer: ");
				list.add(sc.nextInt());
				System.out.println("Added");
			}
			else if(choice ==2){
				// Remove
				System.out.println("Enter the number to remove");
				int elementToRemove = sc.nextInt();
				if(list.contains(elementToRemove)){
					list.remove( Integer.valueOf(elementToRemove));
					System.out.println("Removed. ");
				}else
					System.out.println("Element not found");
			}
			else if(choice == 3){
				System.out.println("Your List: "+ list);
				
			}
			else if(choice == 4){
				System.out.println("Bye");
				break;
			}
		}
		
	}

		public static void displayMenu(){
			System.out.println();
			System.out.println("1. Add");
			System.out.println("2. Remove");
			System.out.println("3. Display");
			System.out.println("4. Exit");
			System.out.println();
			System.out.println("Your choice: ");


		}

}

