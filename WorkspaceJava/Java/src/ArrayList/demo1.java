package ArrayList;

import java.util.ArrayList;
import java.util.Collections;

public class demo1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> list = new ArrayList();
		
		// Adding elements to arraylist 
		list.add(0);
		list.add(2);
		list.add(5);
		System.out.println(list);
		
		// get elements
		int element = list.get(2);
		System.out.println(element);
		
		// add element in between
		list.add(1,1);
		System.out.println(list);
		
		// set element
		list.set(0,5);
		System.out.println(list);
		
		// delete element
		list.remove(3);
		System.out.println(list);
		
		// size
		int size = list.size();
		System.out.println(size);
		
		//loops
		for(int i = 0 ;i<list.size();i++){
			System.out.print(list.get(i));
		}
		System.out.println();
		
		//sorting
		Collections.sort(list);
		System.out.println(list);
	}

}
