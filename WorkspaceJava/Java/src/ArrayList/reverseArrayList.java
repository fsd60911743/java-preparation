package ArrayList;

import java.util.ArrayList;
import java.util.Collections;

public class reverseArrayList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		System.out.println("List before Reverse :"+list);
		
		Collections.reverse(list);
		System.out.println("List after reversed :"+list);
	}

}
