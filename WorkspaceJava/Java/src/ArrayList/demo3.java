package ArrayList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class demo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//using iterator
		
		List<Integer> num = new ArrayList<>();
		num.add(13);
		num.add(23);
		num.add(18);
		num.add(20);
		
		Iterator<Integer> numbersIterator = num.iterator();
		while(numbersIterator.hasNext()){
			Integer nums = numbersIterator.next();
			if(nums%2 !=0){
				numbersIterator.remove();
			}
		}
		System.out.println(num);
	}

}
