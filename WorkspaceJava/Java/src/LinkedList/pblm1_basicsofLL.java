package LinkedList;

import java.util.LinkedList;

public class pblm1_basicsofLL {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList<String> list = new LinkedList<String>();
		System.out.println(list);//[]
		// adding element into linkedlist
		list.add("is");
		//System.out.println(list);
		list.add("a");
		list.addLast("list");
		list.addFirst("this");
		list.add(3, "linked");
		System.out.println(list);
		
		System.out.println(list.get(0));
		System.out.println(list.size());
		list.remove(3);
		list.removeFirst();
		list.removeLast();
		System.out.println(list);
	}

}
