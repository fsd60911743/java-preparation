package LinkedList;

import java.util.LinkedList;

public class prbm_2_ScratchImpnt {
	Node head;
	private int size;
	
	prbm_2_ScratchImpnt(){
		size = 0;
		
	}
	public class Node{
		String data;
		Node next;
		
		Node(String data){
			this.data=data;
			this.next=null;
			size++;
		}
		
	}
	public void addFirst(String data){
		Node newNode= new Node(data);
		newNode.next = head;
		head = newNode;
	}
	public void addLast(String data){
		Node newNode = new Node(data);
		if(head == null){
			head = newNode;
			return;
		}
		Node lastNode = head;
		while(lastNode.next !=null){
			lastNode = lastNode.next;
		}
		lastNode.next = newNode;
	}
	public void printLast(){
		Node currNode = head;
		while(currNode !=null){
			System.out.print(currNode.data+" -> ");
			currNode = currNode.next;
		}
		System.out.println("null");
	}
	public void removeFirst(){
		if(head == null){
			System.out.println("Enter List, noyhing to delete");
			return;
		}
		head=this.head.next;
		size--;
		
	}
	public void removeLast(){
		if(head == null){
			System.out.println("Empty List, nothing to delete");
			return;
		}
		size--;
		if(head.next ==null){
			head = null;
			return;
		}
		
		Node currNode = head;
		Node lastNode = head.next;
		while(lastNode.next !=null){
			currNode = currNode.next;
			lastNode= lastNode.next;
		}
		
		currNode.next=null;
	}
	public int getSize(){
		return size;
		
	}
	
	// insert in the middleof a linked list(at a specified index 'i')?
	public void addMiddle(int index,String data){
		if(index > size || index < 0){
			System.out.println("Invalid Index value");
			return;
		}
		size++;
		
		Node newNode = new Node(data);
		if(head == null || index == 0){
			newNode.next = head;
			head = newNode;
			return;
		}
		Node currNode = head;
		for(int i=1;i<size;i++){
			if(i == index){
				Node nextNode =currNode.next;
				currNode.next = newNode;
				newNode.next = nextNode;
				break;
			}
			currNode = currNode.next;
		}
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		prbm_2_ScratchImpnt list = new prbm_2_ScratchImpnt();
//		list.addLast("is");
//		list.addLast("a");
//		list.addLast("List");
//		list.printLast();
//		list.addFirst("this");
//		list.printLast();
//		System.out.println(list.getSize());
//		list.removeFirst();
//		list.printLast();
//		list.removeLast();
//		list.printLast();
		LinkedList<String>list = new LinkedList<String>();
		
		list.addFirst("Pavan");
		list.addFirst("Name");
		list.addFirst("My");
		System.out.println(list);
		
		list.add(2, "is");
		System.out.println(list);
	}

}
