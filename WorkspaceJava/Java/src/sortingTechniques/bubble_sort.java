package sortingTechniques;

public class bubble_sort {
	
	public static void printArray(int arr[]){
		for(int i = 0 ;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr[] ={7,8,3,1,2};
		
		// time complexity = O(n*2)  it is not the best ways for the sorting the array
		// bubble sort 
		
		for(int i = 0 ;i<arr.length-1;i++){ //n-1 times
			for(int j = 0 ;j<arr.length-i-1 ;j++){ // n-1 , n-2 ... times runs
				if(arr[j]>arr[j+1]){
					// swap
					int temp =arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
			//printArray(arr);
		}
		printArray(arr);
	}

	
}
// time complexcity in worst case is O(n)
