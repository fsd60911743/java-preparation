package strings;

public class reverseString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Approach-1 using charArray 
		String str = "hello";
		char []chArr = str.toCharArray();
		for( int i =chArr.length-1;i>=0;i--){
			System.out.print(chArr[i]);
		}
		System.out.println();
		
		
		// Approach -2 using charAt method
		for( int i = str.length()-1;i>=0 ;i--){
			System.out.print(str.charAt(i));
		}
		System.out.println();
		// Approach-3 using predefine method
		StringBuffer sb = new StringBuffer(str);
		System.out.println(sb.reverse());
		}
	

}
