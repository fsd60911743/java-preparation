package strings;

public class printAtoZ {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		String str = " ";
//		for(int i = 0 ; i<26;i++){
//			char ch = (char)('A'+i);   // this method have time complexity of big O (n2) bcz every time the string is created newly 
//			System.out.println(ch);
//		}
		StringBuilder builder = new StringBuilder();
		for(int i = 0 ;i<26;i++){
			char ch =(char) ('A'+i);
			builder.append(ch);
		}
		System.out.println(builder.toString());
		builder.reverse();
		System.out.println(builder);
	}

}
