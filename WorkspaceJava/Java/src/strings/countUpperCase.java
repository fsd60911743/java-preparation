package strings;

import java.util.Scanner;

public class countUpperCase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
// question -- count only the UPPER CASE character of a String
//			-- count the LOWER CASE character of a String
// 			-- count the DIGIT of a String 
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String");
		String str = sc.next();
		int count= 0,countDigit=0,countlower=0,countSpecialChar=0;
		for(int i = 0 ; i <str.length();i++){
			char c = str.charAt(i);
			if(Character.isUpperCase(c))
				count++;
			if(Character.isLowerCase(c))
				countlower++;
			if(Character.isDigit(c))
				countDigit++;
//			if(Character.isAlphabetic(c))
//				countSpecialChar++;
		}
		System.out.println("Count of UPPERCASE in a string is :"+count);
		System.out.println("count of LOWERCASE in a string is :"+countlower);
		System.out.println("count of Digit in a string is :"+countDigit);
		//System.out.println("count the Special CHaracter in a string :"+countSpecialChar);
	}

}
