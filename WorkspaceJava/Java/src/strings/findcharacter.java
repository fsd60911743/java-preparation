package strings;

public class findcharacter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "Greeks for greeks is a computer science portal";
		int firstIndex = str.indexOf('s');
		System.out.println("First occurrence of char 's'"+"is found at :"+firstIndex);
		
		// returning index of last occurence specified character
		
		int lastIndex = str.lastIndexOf('s');
		System.out.println("last occurrence of char 's' is"+"found at: "+lastIndex );
		
		// returns index of first occurence specified character after the specified index  if found	
		int first_in = str.indexOf('s',10);
		System.out.println("First occurence of char 's'"+" after index 10 :"+first_in);
		
		int last_in =str.lastIndexOf('s', 20);
		System.out.println("Last occurance of char 's'"+" after index 20 is : "+last_in);
		
		//gives ASCII value of character at location 20
		int char_at =str.charAt(20);
		System.out.println("Character at location 20: "+char_at);
		char_at = str.charAt(50);
		System.out.println(char_at);
	}

}
