package strings;
//What is the string pool?
//String pool is nothing but a storage area in Java heap where string literals stores. It is also known as String Intern Pool or String Constant Pool. It is just like object allocation. By default, it is empty and privately maintained by the Java String class. Whenever we create a string the string object occupies some space in the heap memory. Creating a number of strings may increase the cost and memory too which may reduce the performance also.
public class stringpool {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s1 = "Java";
		String s2 = "Java";
		String s3 = new String("Java");
		String s4 = new String("Java").intern();
		System.out.println((s1==s2)+",String are equal.");
		System.out.println((s1==s3)+",String are not equal.");
		System.out.println((s1==s4)+",string are equal.");
	}

}
