package strings;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class freqofChar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		String str = "Football";
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String ");
		String str = sc.next();
		str.toLowerCase();
		
//		Map<Character,Integer> map = new HashMap<Character, Integer>();
//		for(char ch : str.toCharArray()){
//			if(map.get(ch)==null){
//				map.put(ch, 1);
//				
//			}else{
//				map.put(ch,map.get(ch)+1);
//			}
//		}
//		map.forEach((key,value)->{
//			System.out.println(key+" occured "+ value);
//		});
		
		//Method 2
		Map<Character , Integer> charCount = new HashMap<>();
		for(int i =0 ;i<str.length();i++){
			Character ch = str.charAt(i);
			
			if(charCount.containsKey(ch)){
				charCount.put(ch, charCount.get(ch)+1);
			}else{
				charCount.put(ch, 1);
			}
		}
		System.out.println(charCount);
	}

}
