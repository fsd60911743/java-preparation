package strings;

import java.util.Scanner;

public class stringBasics {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//string declaration
//		String name = "pavan";
//		String name2 = "samal";
//		System.out.println(name);
//		System.out.println(name2);
		
		// taking from user input
		
//		Scanner sc = new Scanner(System.in);
//		String name = sc.next();
//		System.out.println(name);
		
		//String functions
		// concatenation
//		String firstName = "Pavan";
//		String lastName = "Samal";
//		String fullName =firstName+lastName;
//		System.out.println(fullName);
//		
//		// length function
//		
//		System.out.println(fullName.length());
//		
//		// charAt function
//		
//		for(int i = 0 ;i<fullName.length();i++){
//			System.out.println(fullName.charAt(i));
//		}
		
		// compare of strings
//		String name1 = "Pavan";
//		String name2 = "Pavan";
//		if(name1.compareTo(name2)==0){
//			System.out.println("Strings are equal");
//		}
//		else {
//			System.out.println("String are not equal");
//		}
//		
		// == operator
//		if(name1 == name2){
//			System.out.println("String are equal");
//		}
//		else{
//			System.out.println("strings are not equal");
//		}
		
//		if(new String("Tony") == new String("Tony")){
//			System.out.println("String are equal");
//		}else{
//			System.out.println("String are NOT equal");
//		}
		// SUBSTRINGS - substrings are nothing but small parts in a string 
		String sentance = "My name is pavan";
//		Substring(beg index , ending index)
		String name =  sentance.substring(5,sentance.length()-2);
		System.out.println(name);
	}

}
