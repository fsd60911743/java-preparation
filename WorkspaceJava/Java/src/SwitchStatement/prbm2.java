package SwitchStatement;

import java.util.Scanner;

public class prbm2 {

	public static void main(String[] args) {
		// to check the grade value of a student using switch statements
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the marks");
		int grade = sc.nextInt();
		if(grade >=0 && grade <=100){
			String letterGrade;
			switch(grade / 10){
			case 10:
			case 9 :
				letterGrade ="A";
				break;
			case 8:
				letterGrade = "B";
				break;
			case 7:
				letterGrade ="C";
				break;
			case 6	:
				letterGrade ="D";
				break;
			default:
				letterGrade="F";
				
			}
			System.out.println("Letter Grade :"+letterGrade);
		}else{
			System.out.println("Invalid Grade");
		}
	}

}
