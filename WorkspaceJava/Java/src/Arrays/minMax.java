package Arrays;

import java.util.Arrays;
import java.util.Scanner;

public class minMax {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		int nums[] = new int[size];
		//int []arr= {1,23,24,46,35,12,24,553,54};
		
		for(int i = 0 ; i<size ; i++){
			nums[i] = sc.nextInt();
		}
		//using inbuild method
		Arrays.sort(nums);
		System.out.println("min-" + nums[0]+" max- " + nums[nums.length-1]);
	}

}
