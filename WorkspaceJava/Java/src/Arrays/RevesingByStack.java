package Arrays;

import java.util.Stack;

public class RevesingByStack {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] arr = {1,2,3,4,5};
		
		reverseArrayUsingStack(arr);
		
		System.out.println("Reversed Array: ");
		for(int element : arr){
			System.out.print(element + " ");
		}
	}

	private static void reverseArrayUsingStack(int[] arr) {
		// TODO Auto-generated method stub
		Stack<Integer> stack = new Stack<>();
		
		for(int element : arr){
			stack.push(element);
		}
		for(int i = 0 ; i<arr.length;i++){
			arr[i] =stack.pop();
		}
	}

}
