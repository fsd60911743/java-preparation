package ExceptionHandling;
// - Exception -exceptions are which we can handle
	//	- we have two types of execptions
	//  1 - checked exception
		// 		- IOExecption
		// 		- SQLExcption

	//  2 - Unchecked exception 
	//          - RuntimeException
	//					- ArrayoutofBounce
	// 					- filenotfound execption
	// 					
// - Errors - we can not handle the errors memory is leacking,cpu is not working
// Exception are which we can handle 
public class DemoException {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int i,j,k=0; // normal statement
		int a[] = new int [4];
		i = 8;
		j =2;
		try{
			k =i/j;  // critical statement
			for(int c=0;c<=4;c++){
				a[c]=c+1;
			}
			for(int value :a){
				System.out.println(value);
			}
			
		}
		catch(ArithmeticException e){
			System.out.println(" cannot divide by Zero"+e);
		}
		catch(ArrayIndexOutOfBoundsException e){
			System.out.println("maximum number of values is 4");
		}
		catch(Exception e){
			System.out.println("Unknown Exception");
		}
		System.out.println(k);
	}

}
