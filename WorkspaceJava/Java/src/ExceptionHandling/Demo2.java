package ExceptionHandling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
// checked exception --  IOException
public class Demo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i ,j=1,k=0;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		i = 8;
		try{
			j=Integer.parseInt(br.readLine());

			k =i/j;
		}
		catch(IOException e){
			System.out.println("some IO Exception");
		}
		catch(Exception e){
			System.out.println("exception "+k);
		}
		System.out.println(k);
	}

}
