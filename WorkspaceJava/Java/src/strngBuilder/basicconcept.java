package strngBuilder;

public class basicconcept {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//String builder is used to create mutable string .
		//the java stringbuilder class is same as stringbuffer class except
		// that it is non synchronized .it is available since jdk 1.5
		
		// stringbuilder declaration
		StringBuilder sb = new StringBuilder("Tony");
		System.out.println(sb);
		
		// setting charAt index
		sb.setCharAt(0,'P');
		System.out.println(sb);
		
		//insert into String 
		sb.insert(2,'n');
		System.out.println("insert the element at index 2 " +sb);
		
		// delete the element from the string
		sb.delete(2, 3);
		System.out.println("Delete the element from the string :"+sb);
		
		// append the string 
		StringBuilder sb1 = new StringBuilder("h");
		sb1.append("e");
		sb1.append("l");
		sb1.append("l");
		sb1.append("o");
		System.out.println(sb1);
		System.out.println("reverse of string using string methods :"+sb1.reverse());
	}

}
